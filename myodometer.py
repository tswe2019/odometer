def new_odometer(n):
    digit = 1;
    odometer = "";
    for digit in range(1,n+1):
        odometer += str(digit);
        digit += 1
    return int(odometer)


#def getReading(odometer):
#    return ''.join(str(digit) for digit in odometer);

def isAscending(reading):   #123 num
    i = 1;
    reading = str(reading)
    for i in range(1,len(reading)):
        if reading[i] <= reading[i-1]:
            return False;
    return True;


def nextReading(reading):
    if (reading == 789):  
        return 123
    reading += 1
    while (not(isAscending(reading))):
        reading += 1;
    return reading;


def prevReading(reading):
    if reading == 123:
        return 789
    reading -= 1;
    while (not(isAscending(reading))):
        reading -= 1
    return reading;


def next_n_readings(reading, n):
    nextnreadings = [];
    for i in range(n):
        reading = nextReading(reading);
        nextnreadings.append(reading);
    return nextnreadings;

def prev_n_readings(reading, n):
    prevnreadings = [];
    for i in range(n):
        reading = prevReading(reading);
        prevnreadings.append(reading);
    return prevnreadings;


def findDistance(reading1, reading2):
    lower_reading = min(reading1,reading2); 
    higher_reading = max(reading1, reading2);
    count = 0;
    while(lower_reading != higher_reading):
        lower_reading = nextReading(lower_reading);
        count += 1
    return count;


def main():
    odometer = new_odometer(4)
    print(prevReading(123));
    print(findDistance(456, 478));
    print(prev_n_readings(678,5));
    print(next_n_readings(456, 8));



if __name__=="__main__":
    main()

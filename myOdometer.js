function odometer(n,lower,upper) {
    var output="";
    min=initialise(n);
    max=maxVal(n);
    count=0;
    for(i=lower;i<upper;i++) {
        if(isAscending(i)) {
            output+=i+" ";
            count++;
        }
    }
    return ("Odometer is initialised to:"+min+"\nThe no of readings between given upper and lower bounds are :"+count+"\nThey are\n"+output);
}

function nextReading(n,reading) {
  
    if(reading == maxVal(n))
        return initialise(n);
      i=reading+1;
    while(!isAscending(i)){
    i++;
    }
    return i;
    
}

function prevReading(n,reading) {
    if(reading == initialise(n))
        return maxVal(n);
    i=reading-1;
    while(!isAscending(i)) {
        i--;
    }
    return i;
}

function initialise(n) {
    var arr=[];
    for(i=1;i<=n;i++)
        arr[i]=i;
    return arr.join("");
}

function maxVal(n) {
    var arr=[];
    for(i=9,j=0;i > 9-n;i--,j++)
        arr[j]=i;
    arr.reverse();
    return (arr.join(""));
}

    
function isAscending(i) {
    var digits=(i).toString().split("").map(Number);
      for(j=0;j<digits.length;j++) {
        if(digits[j] >= digits[j+1])
            return false;
            }
    return true;
}  
print(odometer(4,1234,1785));
print("Next reading of 1389 is "+nextReading(4,1389));
print("Previous reading of 1456 is "+prevReading(4,1456));
